/*
    Acik gorevlerde son gunu bugun olan gorevler kisiye mail olarak atilir.
*/

const axios = require("axios");
//var querystring = require("querystring");
const initMongo = require("./config/mongo");
const Toplanti = require('./models/toplanti')

const Gorev = require('./models/gorev')
const Musteri = require('./models/musteri')
const Personel = require('./models/personel')
const Departman = require('./models/departman')
const moment = require('moment')
const Amqp = require('./utils/amqp')
const Ms = require('./utils/ms')

const tenantId = process.env.TENANT_ID;
const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;


const prepareMessage = (user, toplantilar, yoneticisi) => {
    var message = `<p>Sayın ${user.displayName},</p>
<p></p>
<p>Aşağıda listenelen toplantılar tamamlanmamıştır. Toplantı tarihinden en geç iki gün içinde toplantı kararları girilip kapatılmalıdır.</p>
<br />

`
    var gorevlerTable = `    <table style="width: 100%; border-collapse: collapse;">
<colgroup>
   <col span="1" style="width: 40%;">
   <col span="1" style="width: 20%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 20%;">
   <col span="1" style="width: 10%;">
</colgroup>

<thead>
    <tr>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Toplantı</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Müşteri</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Toplantı Tarihi</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Kategori</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Durum</td>
    </tr>
</thead>
<tbody>
`

    for (const toplanti of toplantilar) {
        gorevlerTable +=
            `            
    <tr style="border-top: thin solid darkgray">
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${toplanti.aciklama}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${toplanti.firma ? toplanti.firma.marka : 'İç Toplantı'}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${moment(toplanti.tarih).format('DD.MM.YYYY')}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${toplanti.kategori}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${toplanti.sonuc == ''? 'Açık' : 'Tamamlandı'}</td>
</tr>
`
    }
    gorevlerTable += `</tbody>
</table>`
    message += gorevlerTable

    return message
}



const tamamlanmamis_toplanti = async () => {
    const token = await Ms.login()
    const users = await Ms.getUserList(token)
    const today = moment().subtract(2, 'days').startOf('day')
    for (const user of users) {

        var yoneticisi = null
        let dep = await Departman.find({personeller: user.id})
        if (dep.length > 0) {
            let parent = await dep[0].getParent()
            if (parent) {
                yoneticisi = parent.yoneticiMsId
            }
        }

        console.log(yoneticisi)



        let toplantilar = await Toplanti.find({
            tarih: {
                //$gte: today.toDate(),
                $lte: moment(today).endOf('day').toDate()
            }, 
            tamamlandi: false, 
            createdByMsId: user.id
        })
        .populate('firma', ['marka'])

        if (toplantilar.length > 0) {
                let message = prepareMessage(user, toplantilar, null)
                Amqp.sendMail('5b487f55-8501-4b6f-9297-f025cd56e1e1', // ExpApp 
                        //'1c88de8b-4d0d-4f59-817c-61f2b76438f0',
                        user.id,
                        {
                            subject: 'Tamamlanmamış toplantılarınız var.',
                            content: message,
                            cc: [
                                //'1c88de8b-4d0d-4f59-817c-61f2b76438f0'
                                yoneticisi,
                                '95fb6b30-e944-42ad-acce-ade94426eadd', // AB
                                '092a66e6-2a24-4b54-aea2-d1658185da35'  // JB
                            ] //yoneticisi
                        }
                    )
        }
    }
}

opertion = async () => {
    await initMongo.connect();
    await tamamlanmamis_toplanti()
    await initMongo.disconnect()
}

opertion()