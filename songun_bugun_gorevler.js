/*
    Acik gorevlerde son gunu bugun olan gorevler kisiye mail olarak atilir.
*/

const axios = require("axios");
//var querystring = require("querystring");
const initMongo = require("./config/mongo");
const Gorev = require('./models/gorev')
const Musteri = require('./models/musteri')
const Personel = require('./models/personel')
const Departman = require('./models/departman')
const moment = require('moment')
const Amqp = require('./utils/amqp')
const Ms = require('./utils/ms')

const tenantId = process.env.TENANT_ID;
const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;


const prepareMessage = (user, gorevler, yoneticisi) => {
    var message = `<p>Sayın ${user.displayName},</p>
<p></p>
<p>Son günü bugün olan görevleriniz aşağıda listelenmiştir.</p>
<br />

`
    var gorevlerTable = `    <table style="width: 100%; border-collapse: collapse;">
<colgroup>
   <col span="1" style="width: 30%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
</colgroup>

<thead>
    <tr>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Görev</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Müşteri</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Son Tarih</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Oluşturma Tarihi</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Görevi oluşturan</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Öncelik</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Kategori</td>
    </tr>
</thead>
<tbody>
`

    for (const gorev of gorevler) {
        gorevlerTable +=
            `            
    <tr style="border-top: thin solid darkgray">
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.baslik}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.musteri ? gorev.musteri.marka : 'İç Görev'}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${moment(gorev.sonTarih).format('DD.MM.YYYY')}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${moment(gorev.createdAt).format('DD.MM.YYYY')}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.olusturanPersonel.displayName}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${gorev.oncelik}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${gorev.gorevKategori}</td>
</tr>
`
    }
    gorevlerTable += `</tbody>
</table>`
    message += gorevlerTable

    return message
    //console.log(message)



}



const songun_bugun_gorevler = async () => {
    const token = await Ms.login()
    const users = await Ms.getUserList(token)
    const today = moment().startOf('day')

    for (const user of users) {
        let gorevler = await Gorev.find({
            atananMsId: user.id, sonuc: 'acik', sonTarih: {
                $gte: today.toDate(),
                $lte: moment(today).endOf('day').toDate()
            }
        }).populate('musteri')
        if (gorevler.length > 0) {

            for (var gorev of gorevler) {
                let olusturan = users.find(user => user.id == gorev.createdByMsId)
                gorev.olusturanPersonel = olusturan
            }

            let message = prepareMessage(user, gorevler, null)
            console.log(message)
            Amqp.sendMail('5b487f55-8501-4b6f-9297-f025cd56e1e1', // ExpApp 
                //'1c88de8b-4d0d-4f59-817c-61f2b76438f0', 
                user.id,
                {
                    subject: 'Son günü bugün olan görevleriniz var.',
                    content: message,
                    cc: [
                        //'1c88de8b-4d0d-4f59-817c-61f2b76438f0'
                        //yoneticisi,
                        //'95fb6b30-e944-42ad-acce-ade94426eadd', // AB
                        //'092a66e6-2a24-4b54-aea2-d1658185da35'  // JB
                    ] //yoneticisi
                }
            )
        }

    }
}

opertion = async () => {
    await initMongo.connect();
    await songun_bugun_gorevler()
    await initMongo.disconnect()
}

opertion()