const axios = require("axios");
var querystring = require("querystring");
const initMongo = require("./config/mongo");
const Gorev = require('./models/gorev')
const Musteri = require('./models/musteri')
const Personel = require('./models/personel')
const Departman = require('./models/departman')
const moment = require('moment')
const Amqp = require('./utils/amqp')


const tenantId = process.env.TENANT_ID;
const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;


const prepareMessage = (user, gorevler, yoneticisi) => {
    var message = `<p>Sayın ${user.displayName},</p>
<p></p>
<p>Geciken görevleriniz aşağıda listelenmiştir.</p>
<br />

`
    var gorevlerTable = `    <table style="width: 100%; border-collapse: collapse;">
<colgroup>
   <col span="1" style="width: 30%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
   <col span="1" style="width: 10%;">
</colgroup>

<thead>
    <tr>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Görev</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Müşteri</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Son Tarih</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Oluşturma Tarihi</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Gecikme</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Görevi oluşturan</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Öncelik</td>
        <td style="background-color: #aaa; text-align: center; padding: 10px; font-family:Arial, sans-serif;font-size:16px;">Kategori</td>
    </tr>
</thead>
<tbody>
`

    for (const gorev of gorevler) {
        //console.log(gorev)
        gorevlerTable +=
            `            
    <tr style="border-top: thin solid darkgray">
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.baslik}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.musteri ? gorev.musteri.marka : 'İç Görev'}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${moment(gorev.sonTarih).format('DD.MM.YYYY')}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${moment(gorev.createdAt).format('DD.MM.YYYY')}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${moment(new Date()).diff(gorev.sonTarih, 'days')} gün</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;">${gorev.olusturanPersonel.displayName}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${gorev.oncelik}</td>
    <td style="padding: 10px; font-family: Arial, Helvetica, sans-serif;text-align: center;">${gorev.gorevKategori}</td>
</tr>
`
    }
    gorevlerTable += `</tbody>
</table>`
    message += gorevlerTable

    //console.log(message)


    Amqp.sendMail('5b487f55-8501-4b6f-9297-f025cd56e1e1', // ExpApp 
        //'1c88de8b-4d0d-4f59-817c-61f2b76438f0', 
        user.id, 
        {
        subject: 'Geciken görevleriniz var.',
        content: message,
        cc: [
            //'1c88de8b-4d0d-4f59-817c-61f2b76438f0'
            yoneticisi, 
            '95fb6b30-e944-42ad-acce-ade94426eadd', // AB
            '092a66e6-2a24-4b54-aea2-d1658185da35'  // JB
        ] //yoneticisi
    }
    )

}

const progress = async () => {
    // 1. Tum kisiler listesi alinir
    const respLogin = await axios.post(
        `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
        querystring.stringify({
            client_id: clientId,
            scope: "https://graph.microsoft.com/.default",
            client_secret: clientSecret,
            grant_type: "client_credentials",
        }),
        {
            headers: { "Content-Type": "application/x-www-form-urlencoded" },
        }
    );

    const usersReps = await axios.get(`https://graph.microsoft.com/v1.0/users`,
        { headers: { Authorization: "Bearer " + respLogin.data.access_token } }
    )

    const users = usersReps.data.value

    // 2. Kisilerin gecikmis gorevleri toplanir.
    for (let user of users) {
        // yoneticisini bul
        var yoneticisi = null
        let dep = await Departman.find({personeller: user.id})
        if (dep.length > 0) {
            let parent = await dep[0].getParent()
            if (parent) {
                yoneticisi = parent.yoneticiMsId
            }
        }
        //console.log(yoneticisi)
        //if (yoneticisi) {
        //    let u = users.find(user => user.id == yoneticisi)
        //    console.log(`${user.displayName} - ${u.displayName} `)
       // }

        let gorevler = await Gorev.find({ atananMsId: user.id, sonuc: 'acik' }).sort('sonTarih').populate('musteri')

        var acikGorevler = []

        for (let _gorev of gorevler) {
            let gorev = _gorev.toObject()
            let sonTarih = new Date(gorev.sonTarih)
            sonTarih.setHours(0, 0, 0, 0)
            let now = new Date()
            now.setHours(0, 0, 0, 0)
            if (sonTarih < now) {
                let olusturan = users.filter(user => user.id == gorev.createdByMsId)
                acikGorevler.push({ ...gorev, olusturanPersonel: olusturan[0] })
            }
        }
        if (acikGorevler.length > 0) {
            prepareMessage(user, acikGorevler, yoneticisi)
        }
    }
}


opertion = async () => {
    await initMongo.connect();
    await progress()
    await initMongo.disconnect()
}

opertion()