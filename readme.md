Rutin islemler

1. Gecikmis gorevler mail atilir.
    Her personelin ACIK olan ve sonTarihi gecmis olan gorevleri listelenerek
    ExpertoApp tarafindan kendisine ve yonetici, Alpaslan ve Julide CC'de mail atilir.
    Bu islem her gun 05:00'da yapilir.

    crontab -e
    00 04 * * * cd /root/experto-rutin && yarn start
    
    //pm2 start x.js --cron "00 04 * * *"

2. Son günü bugün olan görevler mail atılır.
    yarn songun
        
    crontab -e
    00 05 * * * cd /root/experto-rutin && yarn songun

3. Toplantı tarihinden itibaren 2 gün içinte toplantı notları girilmiş olmalı. 
    Toplantı tarihinden 2 gün sonra açık toplantı varsa Toplantıyı oluşturanalra uyarı maili gider.

    yarn tamamlanmamistoplanti
        
    crontab -e
    30 05 * * * cd /root/experto-rutin && yarn tamamlanmamistoplanti
