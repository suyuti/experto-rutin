const axios = require("axios");
var querystring = require("querystring");

const tenantId = process.env.TENANT_ID;
const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;

var accessToken = null

const login = async () => {
    const respLogin = await axios.post(
        `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
        querystring.stringify({
            client_id: clientId,
            scope: "https://graph.microsoft.com/.default",
            client_secret: clientSecret,
            grant_type: "client_credentials",
        }),
        {
            headers: { "Content-Type": "application/x-www-form-urlencoded" },
        }
    );

    if (respLogin.data) {
        accessToken = respLogin.data.access_token
        return respLogin.data.access_token
    }
}

const getUserList = async (token) => {
    const usersReps = await axios.get(`https://graph.microsoft.com/v1.0/users`,
        { headers: { Authorization: "Bearer " + token } }
    )
    return usersReps.data.value
}

module.exports ={
    login,
    getUserList
}