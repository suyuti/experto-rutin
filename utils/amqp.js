const amqplib = require("amqplib/callback_api");
const amqp = process.env.AMQP_URL;
const mail_queue = process.env.QUEUE_MAIL;

const sendMail = async (fromMsId, toMsId, mailObj) => {
  amqplib.connect(amqp, (err, connection) => {
    if (err) {
      console.error(err.stack);
      return; //process.exit(1);
    }

    connection.createChannel((err, channel) => {
      if (err) {
        console.error(err.stack);
        return; //process.exit(1);
      }

      channel.assertQueue(mail_queue, { durable: true }, (err) => {
        if (err) {
          console.error(err.stack);
          return; //process.exit(1);
        }

        let sender = (content) => {
          let sent = channel.sendToQueue(
            mail_queue,
            Buffer.from(JSON.stringify(content)),
            {
              persistent: true,
              contentType: "application/json",
            }
          );
          if (sent) {
            return channel.close(() => connection.close());
          } else {
            channel.once("drain", () => next());
          }
        };

        sender({
          fromMsId: fromMsId,
          toMsId: toMsId,
          mailObj: mailObj,
        });
      });
    });
  });
};

module.exports = {
  sendMail,
};
