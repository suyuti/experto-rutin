const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const musteriDurumlari = ['aktif', 'pasif', 'potansiyel', 'sorunlu']

var SchemaModel = new Schema(
  {
      marka             : {type: String, required: true},
      unvan             : {type: String},
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports = mongoose.models.Musteri ||  mongoose.model("Musteri", SchemaModel);
