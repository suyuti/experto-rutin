const mongoose = require("mongoose");
const MpathPlugin = require ('mongoose-mpath');

const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    adi         : { type: String },
    yoneticiMsId: { type: String },
    yoneticisi  : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    personeller : [ String ]
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports =
 mongoose.models.Departman || mongoose.model("Departman", SchemaModel.plugin(MpathPlugin));
