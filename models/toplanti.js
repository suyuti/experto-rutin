const mongoose = require("mongoose");

const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    toplantimsid  : String,
    baslik        : { type: String, required: true },
    aciklama      : String,
    tarih         : Date,
    saat          : String, 
    yer           : String,
    sure          : String,
    kategori      : String,
    gundem        : String,
    tamamlandi          : { type: Boolean, default: false},
    tamamlandiTarih     : Date,
    tamamlandiUserMsId  : String,
    attachments       : [ {type: mongoose.Schema.Types.ObjectId, ref: 'File'}],
    onayReq           : { type: Boolean, default: false},    

    tur           : {type: String, enum: ['yuzyuze', 'telekonferans', 'videokonferans']},
    firma         : { type: mongoose.Schema.Types.ObjectId, ref: "Musteri" },
    expertoKatilimcilar: [String],
    expertoKatilimcilarDisplayName: [String],
    //firmaKatilimcilar: [String],
    firmaKatilimcilar: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Kisi'}],
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: "Personel" },
    createdByMsId: String, // olusturan kisinin MSID
    createdAt: Date,
    updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: "Personel" },
    updatedAt: Date,
    sonuc: String,
    kararlar: [{ type: mongoose.Schema.Types.ObjectId, ref: "ToplantiKarar" }],
    history: [
      {
        eylem: String,
        tarih: Date,
        olusturan: { type: mongoose.Schema.Types.ObjectId, ref: "Personel" },
        msid: String,
      },
    ],
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports =
  mongoose.models.Toplanti || mongoose.model("Toplanti", SchemaModel);
