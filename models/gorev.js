const mongoose = require("mongoose");

const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    baslik            : { type: String, required: true  },
    aciklama          : { type: String, required: true  },
    baslamaTarihi     : { type: Date  },
    bitisTarihi       : { type: Date  },
    sonTarih          : { type: Date  },
    icGorevMi         : Boolean,
    oncelik           : { type: String,   },
    tahminiSaat       : Number, // Saat cinsinden
    gorevKategori     : String,
    onayReq           : { type: Boolean, default: false},    
    attachments       : [ {type: mongoose.Schema.Types.ObjectId, ref: 'File'}],
    
    //atananlar         : [{ type: mongoose.Schema.Types.ObjectId, ref: 'Personel'  }],
    //atananlar         : [{ type: String  }], // Microsoft IDleri tutuluyor. Personel iliskisi kurulamaz
    
    atananMsId        : String,
    atanan            : {type: mongoose.Schema.Types.ObjectId, ref: 'Personel'},
    toplantiKarar     : { type: mongoose.Schema.Types.ObjectId, ref: 'ToplantiKarar'},
    ilgiliToplanti    : { type: mongoose.Schema.Types.ObjectId, ref: 'Toplanti'},
    
    createdBy         : { type: mongoose.Schema.Types.ObjectId, ref: 'Personel'  },
    createdByMsId     : String, // olusturan kisinin MSID
    updatedBy         : { type: mongoose.Schema.Types.ObjectId, ref: 'Personel'  },
    createdAt         : { type: Date  },
    updatedAt         : { type: Date  },
    musteri           : { type: mongoose.Schema.Types.ObjectId, ref: 'Musteri'  },
    sonuc             : String,
    sonucAciklama     : String,
    sonucTarihi       : Date,
    yorumlar          : [
      {
        yorum             : { type: String }, 
        olusturan         : { type: mongoose.Schema.Types.ObjectId, ref: 'Personel'  },
        olusturmaTarihi   : { type: Date  },
      }
    ],
    history           : [
      {
        eylem     : String,
        tarih     : Date,
        olusturan : { type: mongoose.Schema.Types.ObjectId, ref: 'Personel'  },
        msid      : String
      }
    ]

  },
  {
    versionKey: false,
    timestamps: true,
  }
);

SchemaModel.pre("save", function (next) {
    return next();
});

module.exports = mongoose.models.Gorev || mongoose.model("Gorev", SchemaModel);
